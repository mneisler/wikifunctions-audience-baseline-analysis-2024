# wikifunctions-audience-baseline-analysis-2024

Develop a baseline understanding of wikifunction users and their motivations. Insights will be used to identify opportunities for community and diversity growth.

[Task](https://phabricator.wikimedia.org/T355810)